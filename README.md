# README #

1/3スケールのNEC PC-8001mk2風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1983年1月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-8001mkII)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/5ba8ba5df311d049d247de21dce4aef5)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2/raw/6c1b1d25d219fccb265f2a74fdca91111642e12b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2/raw/6c1b1d25d219fccb265f2a74fdca91111642e12b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2/raw/6c1b1d25d219fccb265f2a74fdca91111642e12b/ExampleImage.png)
